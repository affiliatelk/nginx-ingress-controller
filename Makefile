# https://github.com/kubernetes/ingress/tree/master/examples/deployment/nginx
# https://github.com/kubernetes/ingress/tree/master/examples/rbac/nginx

disable-legacy-authorization:
	gcloud container clusters update cluster --zone asia-east1-c --no-enable-legacy-authorization


create: create-rbac-permissions create-default-backend create-controller

create-rbac-permissions:
	kubectl apply -f nginx-ingress-controller-rbac.yml

create-default-backend:
	kubectl apply -f default-backend.yaml

create-controller:
	kubectl apply -f nginx-ingress-controller.yaml

kube-system-pods:
	kubectl -n kube-system get pods